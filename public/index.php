<?php
	$link_info = array(
		array(
			"id" => "link01",  // javascriptで遷移させるためのid
			"url" => "/game.php",
			"title" => "GAME",
			"color" => "white",
			"bgcolor" => "#369CBA",
			"icon" => "gamepad",  // font-awesomeを表示するためのkey
		),
	);
	$css_files = array(
		"css/common.css",
		"css/top.css",
	);
	$js_files = array(
		"js/top.js",
	);
?>
<?php include("../include/template/header.tpl.php"); ?>
	<div class="container">
		<div id="balloon_container">
			<div id="balloon">
				<p id="message"></p>
			</div>
		</div>
		<canvas id="beacchi" width=196 height=265></canvas>
		<canvas id="palette" style="display:none;"></canvas>
		<div class="links">
<?php foreach ($link_info as $l) { ?>
			<div class="link" id="<?= $l["id"] ?>" style="color: <?= $l["color"] ?>; background-color:<?= $l["bgcolor"] ?>;" onclick="location.href='<?= $l["url"] ?>';">
				<i class="fa fa-<?= $l["icon"] ?>"></i><span><?= $l["title"] ?></span>
			</div>
<?php } ?>
		</div>
	</div>
<?php include("../include/template/footer.tpl.php"); ?>

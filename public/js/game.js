$(function() {
	// requestAnimationFrameのベンダー依存を吸収
	var requestAnimationFrame = window.requestAnimationFrame ||
															window.mozRequestAnimationFrame ||
															window.webkitRequestAnimationFrame ||
															window.msRequestAnimationFrame;
	window.requestAnimationFrame = requestAnimationFrame;

	// cancelAnimationFrameのベンダー依存を吸収
	var cancelAnimationFrame = window.cancelAnimationFrame ||
														window.mozcancelAnimationFrame ||
														window.webkitcancelAnimationFrame ||
														window.mscancelAnimationFrame;
	window.cancelAnimationFrame = cancelAnimationFrame;

	//**************************************************
	// 各グローバル変数用意
	//**************************************************
	// canvasフレーム数
	var frameNow = 0;

	// 描画用canvas要素取得
	var beacchiDom = $("#beacchi");
	var beacchiCanvas = beacchiDom[0];
	var beacchiCtx = beacchiCanvas.getContext('2d');

	// 画像編集用canvas要素取得
	var paletteCanvas = document.getElementById('palette');
	var paletteCtx = paletteCanvas.getContext('2d');

	// 画像
	var beacchiImg = new Image();
	var tobibakoImg = new Image();
	var humikomiImg = new Image();

	// 素材格納庫
	var materials = [];

	// 各素材
	var matBeacchi = null; // ベアっち
	var matTobibako = null; // 跳び箱
	var matHumikomi = null; // 踏み込み

	// 素材雛形
	var Material = function (img, dx, dy, dw=null, dh=null, sx=null, sy=null, sw=null, sh=null, deg=0, axis="centermiddle") {
		this.frameInit = 0; // canvasに追加された時のフレーム数
		this.frameCount = 0; // canvasに追加されてから現在時刻までのフレームカウント数
		this.img = img;
		this.imgDx = dx; // 描画イメージ矩形のx座標
		this.imgDy = dy; // 描画イメージ矩形のy座標
		this.imgDw = (dw !== null) ? dw : img.width;  // イメージを描画する幅（初期値はイメージ本来の幅）
		this.imgDh = (dh !== null) ? dh : img.height; // イメージを描画する高さ（初期値はイメージ本来の高さ）
		this.imgSx = (sx !== null) ? sx : 0; // 元イメージ使用範囲の矩形のx座標（初期値は0）
		this.imgSy = (sy !== null) ? sy : 0; // 元イメージ使用範囲の矩形のy座標（初期値は0）
		this.imgSw = (sw !== null) ? sw : img.width;  // 元イメージ使用範囲の矩形の幅（初期値はイメージ本来の幅）
		this.imgSh = (sh !== null) ? sh : img.height; // 元イメージ使用範囲の矩形の高さ（初期値はイメージ本来の高さ）
		this.deg = deg; // 画像の回転角度
		this.axis = axis; // 画像の回転軸
		this.byebyeFlag = false; // 素材格納庫から出してくれの合図用フラグ
		// canvasに追加された時に呼ばれるメソッド
		this.attached = function () {
			// 初期フレーム数を記録
			this.frameInit = frameNow;
		};
		// 経過フレーム数を算出
		this.countFrame = function () {
			this.frameCount = frameNow - this.frameInit;
		};
		// フレームカウントに合わせて位置などを再設定するメソッド
		this.refreshParam = function () {
		};
		// 各パラメータを初期値に戻すメソッド
		this.initializeParam = function () {
		};
		this.byebye = function () {
			this.byebyeFlag = true;
		};
	};

	//**************************************************
	// 画像ロード後、処理開始
	//**************************************************
	preloadImage(afterPreload);

	// 画像プリロード
	function preloadImage(callback) {
		var preloadCounter = clPreloadCounter(3, callback);

		beacchiImg.src = "img/beacchi.png";
		tobibakoImg.src = "img/tobibako.png";
		humikomiImg.src = "img/humikomi.png";

		beacchiImg.onload = preloadCounter;
		tobibakoImg.onload = preloadCounter;
		humikomiImg.onload = preloadCounter;
	}

	// プリロードカウンター(クロージャ)
	function clPreloadCounter(expectedCnt, callback) {
		var cnt = 0;
		return function () {
			cnt++;
			if (cnt == expectedCnt) {
				callback();
			}
		};
	}

	// 画像読み込み完了後の処理
	function afterPreload () {
		//**************************************************
		// 各素材の作成、動作の定義
		//**************************************************
		// 素材（ベアっち）
		matBeacchi = new Material(beacchiImg, 0, 0);  

		addMaterial(matBeacchi);
		loop();
	}

	// 再描画ループ
	function loop() {
		requestAnimationFrame(loop);
		frameNow++;
		// ベアっちキャンバス初期化
		beacchiCtx.beginPath();
		beacchiCtx.clearRect(0, 0, beacchiCanvas.width, beacchiCanvas.height);
		// 素材を描画
		_.each(materials, function (elm, i) {
			elm.countFrame(); // 素材ごとの経過フレーム数を算出
			elm.refreshParam(); // 経過フレーム数に応じて移動など
			if (elm.deg === 0) {
				beacchiCtx.drawImage(elm.img, elm.imgSx, elm.imgSy, elm.imgSw, elm.imgSh, elm.imgDx, elm.imgDy, elm.imgDw, elm.imgDh);
			} else {
				// 画像編集用キャンバス初期化
				paletteCtx.beginPath();
				paletteCtx.clearRect(0, 0, paletteCanvas.width, paletteCanvas.height);
				// 画像投入
				paletteCtx.drawImage(elm.img, 0, 0);
				// 回転などのパラメータ保持
				paletteCtx.restore();
				paletteCtx.save();
				// 回転軸の定義
				var axisMoveX = 0;
				var axisMoveY = 0;
				switch (elm.axis) {
					case 'centermiddle':
						axisMoveX = elm.img.width/2;
						axisMoveY = elm.img.height/2;
						break;
					case 'centertop':
						axisMoveX = elm.img.width/2;
						axisMoveY = 0;
						break;
				}
				// 軸を中心にしてから回転し、軸を元に戻す
				paletteCtx.translate(axisMoveX, axisMoveY);
				paletteCtx.rotate(elm.deg * Math.PI / 180);
				paletteCtx.translate(-1*axisMoveX, -1*axisMoveY);
				// 回転後の画像を描画
				beacchiCtx.drawImage(paletteCanvas, elm.imgSx, elm.imgSy, elm.imgSw, elm.imgSh, elm.imgDx, elm.imgDy, elm.imgDw, elm.imgDh);
			}
		});
	}

	//**************************************************
	// 関数群
	//**************************************************
	// ベアっち初期化
	function initializeBeacchi () {
		removeAllMaterial();
		addMaterial(matBeacchi);
	}

	// 描画素材追加関数
	function addMaterial (mat) {
		mat.attached();
		mat.initializeParam();
		materials.push(mat);
	}

	// 描画素材を空に
	function removeAllMaterial () {
		materials = [];
	}
});

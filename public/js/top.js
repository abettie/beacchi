$(function() {
	// requestAnimationFrameのベンダー依存を吸収
	var requestAnimationFrame = window.requestAnimationFrame ||
															window.mozRequestAnimationFrame ||
															window.webkitRequestAnimationFrame ||
															window.msRequestAnimationFrame;
	window.requestAnimationFrame = requestAnimationFrame;

	// cancelAnimationFrameのベンダー依存を吸収
	var cancelAnimationFrame = window.cancelAnimationFrame ||
														window.mozcancelAnimationFrame ||
														window.webkitcancelAnimationFrame ||
														window.mscancelAnimationFrame;
	window.cancelAnimationFrame = cancelAnimationFrame;

	//**************************************************
	// 各グローバル変数用意
	//**************************************************
	// canvasフレーム数
	var frameNow = 0;

	// 描画用canvas要素取得
	var beacchiDom = $("#beacchi");
	var beacchiCanvas = beacchiDom[0];
	var beacchiCtx = beacchiCanvas.getContext('2d');

	// 画像編集用canvas要素取得
	var paletteCanvas = document.getElementById('palette');
	var paletteCtx = paletteCanvas.getContext('2d');

	// 画像
	var beacchiImg = new Image();
	var kiranImg = new Image();
	var tearImg = new Image();
	var tongueImg = new Image();
	var saaaImg = new Image();
	var angryImg = new Image();

	// 素材格納庫
	var materials = [];

	// 各素材
	var matBeacchi = null; // ベアっち
	var matKiran = null; // キラン☆
	var matTear1 = null; // うるうる
	var matTear2 = null; // うるうる
	var matTongue = null; // あっかんべー
	var matSaaa = null; // サー
	var matAngry = null; // プンプン

	// 素材雛形
	var Material = function (img, dx, dy, dw=null, dh=null, sx=null, sy=null, sw=null, sh=null, deg=0, axis="centermiddle") {
		this.frameInit = 0; // canvasに追加された時のフレーム数
		this.frameCount = 0; // canvasに追加されてから現在時刻までのフレームカウント数
		this.img = img;
		this.imgDx = dx; // 描画イメージ矩形のx座標
		this.imgDy = dy; // 描画イメージ矩形のy座標
		this.imgDw = (dw !== null) ? dw : img.width;  // イメージを描画する幅（初期値はイメージ本来の幅）
		this.imgDh = (dh !== null) ? dh : img.height; // イメージを描画する高さ（初期値はイメージ本来の高さ）
		this.imgSx = (sx !== null) ? sx : 0; // 元イメージ使用範囲の矩形のx座標（初期値は0）
		this.imgSy = (sy !== null) ? sy : 0; // 元イメージ使用範囲の矩形のy座標（初期値は0）
		this.imgSw = (sw !== null) ? sw : img.width;  // 元イメージ使用範囲の矩形の幅（初期値はイメージ本来の幅）
		this.imgSh = (sh !== null) ? sh : img.height; // 元イメージ使用範囲の矩形の高さ（初期値はイメージ本来の高さ）
		this.deg = deg; // 画像の回転角度
		this.axis = axis; // 画像の回転軸
		this.byebyeFlag = false; // 素材格納庫から出してくれの合図用フラグ
		// canvasに追加された時に呼ばれるメソッド
		this.attached = function () {
			// 初期フレーム数を記録
			this.frameInit = frameNow;
		};
		// 経過フレーム数を算出
		this.countFrame = function () {
			this.frameCount = frameNow - this.frameInit;
		};
		// フレームカウントに合わせて位置などを再設定するメソッド
		this.refreshParam = function () {
		};
		// 各パラメータを初期値に戻すメソッド
		this.initializeParam = function () {
		};
		this.byebye = function () {
			this.byebyeFlag = true;
		};
	};

	// セリフデータ
	var serifDataOrg;

	// 表示順制御用セリフデータ
	var serifData;

	//**************************************************
	// json, 画像ロード後、処理開始
	//**************************************************
	// セリフデータ読み込み
	var jsonfilePath = "data/serifs.json";
	$.getJSON(jsonfilePath, function (json) {
		serifDataOrg = json;
		// 画像も読み込んでおく
		preloadImage(afterPreload);
	});

	// 画像プリロード
	function preloadImage(callback) {
		var preloadCounter = clPreloadCounter(6, callback);

		beacchiImg.src = "img/beacchi.png";
		kiranImg.src = "img/kiran.png";
		tearImg.src = "img/tear.png";
		tongueImg.src = "img/tongue.png";
		saaaImg.src = "img/saaa.png";
		angryImg.src = "img/angry.png";

		beacchiImg.onload = preloadCounter;
		kiranImg.onload = preloadCounter;
		tearImg.onload = preloadCounter;
		tongueImg.onload = preloadCounter;
		saaaImg.onload = preloadCounter;
		angryImg.onload = preloadCounter;
	}

	// プリロードカウンター(クロージャ)
	function clPreloadCounter(expectedCnt, callback) {
		var cnt = 0;
		return function () {
			cnt++;
			if (cnt == expectedCnt) {
				callback();
			}
		};
	}

	// 画像読み込み完了後の処理
	function afterPreload () {
		// セリフ初期投入	
		serifSoldOut();
		// クリックイベント設定
		beacchiDom.on('mousedown', beacchiMousedown);
		beacchiDom.on('mouseup', beacchiMouseup);

		//**************************************************
		// 各素材の作成、動作の定義
		//**************************************************
		// 素材（ベアっち）
		matBeacchi = new Material(beacchiImg, 0, 0);  

		// 素材（うるうる）
		// ※初めは高さ0で作成
		matTear1 = new Material(tearImg, 60, 73, null, 0, null, tearImg.height, null, 0);  
		matTear1.bound = false; // バウンド動作開始フラグ
		matTear1.boundStartFrame = 0; // バウンド開始時のフレーム数
		matTear1.refreshParam = function () {
			if (this.imgSh < this.img.height && this.bound !== true) {
				// 一番下までは画像がトイレットペーパーみたいに下降。
				var amount = this.frameCount * 3; // 変化量
				this.imgDh = this.imgSh = amount;
				this.imgSy = this.img.height - amount;
				this.boundStartFrame = this.frameCount;
			} else {
				// 下まで行ったらバウンドし続ける
				this.bound = true;
				// 経過フレーム数からバウンド表現用ラジアンとバウンド幅算出
				var boundFrameCount = this.frameCount - this.boundStartFrame;
				var rad = boundFrameCount * 4 * Math.PI / 180;
				var boundWidth = 20 - boundFrameCount / 15
				if (boundWidth < 0 ) {
					boundWidth = 0;
				}

				var amount = Math.abs(Math.sin(rad)) * boundWidth;
				this.imgDh = this.imgSh = this.img.height - amount;
				this.imgSy = amount;
			}
		};
		console.log(matTear1);
		console.log(matTear1.constructor);
		console.log(matTear1.__proto__);
		console.log(matTear1.__proto__.__proto__);
		matTear1.initializeParam = function () {
			this.bound = false;
			this.boundStartFrame = 0;
			this.imgDh = this.imgSh = 0;
			this.imgSy = this.img.height;
		}
		// 2つ目は1つ目をベースに
		matTear2 = $.extend(true, [], matTear1);
		matTear2.imgDx = 100;
		matTear2.imgDy = 73;

		// 素材（キラン☆）
		matKiran = new Material(kiranImg, 112, 47);  
		matKiran.sinDegKaiten = 0;
		matKiran.sinDegKakudai = 0;
		matKiran.sinDegKakudai2 = 180;
		matKiran.initialImgDx = matKiran.imgDx;
		matKiran.initialImgDy = matKiran.imgDy;
		matKiran.refreshParam = function () {
			// くるくる回転
			if (this.sinDegKaiten !== 180) {
				this.deg += 10 * Math.sin(this.sinDegKaiten * Math.PI / 180);
				this.sinDegKaiten += 6;
			}
			if (this.sinDegKakudai !== 180) {
				// ボワッと拡大
				var additionalWidth = this.img.width * 0.7 * Math.sin(this.sinDegKakudai * Math.PI / 180);
				var additionalHeight = this.img.height * 0.7 * Math.sin(this.sinDegKakudai * Math.PI / 180);
				this.imgDw = this.img.width + additionalWidth;
				this.imgDh = this.img.height + additionalHeight;
				this.imgDx = this.initialImgDx - additionalWidth / 2;
				this.imgDy = this.initialImgDy - additionalHeight / 2;
				this.sinDegKakudai += 6;
			} else {
				// 小さい収縮拡大を繰り返す
				var additionalWidth = this.img.width * 0.2 * Math.sin(this.sinDegKakudai2 * Math.PI / 180);
				var additionalHeight = this.img.height * 0.2 * Math.sin(this.sinDegKakudai2 * Math.PI / 180);
				this.imgDw = this.img.width + additionalWidth;
				this.imgDh = this.img.height + additionalHeight;
				this.imgDx = this.initialImgDx - additionalWidth / 2;
				this.imgDy = this.initialImgDy - additionalHeight / 2;
				this.sinDegKakudai2 += 4;
			}
		}
		matKiran.initializeParam = function () {
			this.deg = 0;
			this.sinDegKaiten = 0;
			this.sinDegKakudai = 0;
			this.sinDegKakudai2 = 180;
		}

		// 素材（あっかんべー）
		matTongue = new Material(tongueImg, 72, 97, null, 0, null, tongueImg.height, null, 0, 0, 'centertop');  
		matTongue.swayStartFrame = 0;
		matTongue.refreshParam = function () {
			if (this.imgSh < this.img.height) {
				// 一番下までは画像がトイレットペーパーみたいに下降。
				var amount = this.frameCount * 2; // 変化量
				this.imgDh = this.imgSh = amount;
				this.imgSy = this.img.height - amount;
				this.swayStartFrame = this.frameCount;
				this.deg = 1; // 下降から回転に移った時の画像乱れ防止
			} else {
				// ゆらゆら揺れる
				var swayFrameCount = this.frameCount - this.swayStartFrame;
				this.deg = 20 * Math.sin(swayFrameCount * 5 * Math.PI / 180);
			}
		};
		matTongue.initializeParam = function () {
			this.deg = 0;
			this.imgDh = this.imgSh = 0;
			this.imgSy = this.img.height;
			this.swayFrameCount = 0;
		}

		// 素材（血の気が引く）
		matSaaa = new Material(saaaImg, 72, 28);  
		matSaaa.refreshParam = function () {
			if (this.imgSh < this.img.height) {
				// 一番下までは画像がトイレットペーパーみたいに下降。
				var amount = 50 + this.frameCount * 2; // 変化量
				this.imgDh = this.imgSh = amount;
				this.imgSy = this.img.height - amount;
			}
		};
		matSaaa.initializeParam = function () {
			this.imgDh = this.imgSh = 0;
			this.imgSy = this.img.height;
		}

		// 素材（プンプン）
		matAngry = new Material(angryImg, 108, 32);  
		matAngry.reductCount = 0;
		matAngry.initialImgDx = matAngry.imgDx;
		matAngry.initialImgDy = matAngry.imgDy;
		matAngry.refreshParam = function () {
			if (this.reductCount <= 7 && Math.floor(this.frameCount / 5) % 2 === 0) {
				// 縮小
				//console.log(this.frameCount);
				var additionalWidth = -10;
				var additionalHeight = -10;
				this.imgDw = this.img.width + additionalWidth;
				this.imgDh = this.img.height + additionalHeight;
				this.imgDx = this.initialImgDx - additionalWidth / 2;
				this.imgDy = this.initialImgDy - additionalHeight / 2;
				this.reductCount += 1;
			} else {
				this.imgDw = this.img.width;
				this.imgDh = this.img.height;
				this.imgDx = this.initialImgDx;
				this.imgDy = this.initialImgDy;
			}
		}
		matAngry.initializeParam = function () {
			this.reductCount = 0;
		}

		// 初回表示
		beacchiMousedown();
		beacchiMouseup();

		loop();
	}

	// 再描画ループ
	function loop() {
		requestAnimationFrame(loop);
		frameNow++;
		// ベアっちキャンバス初期化
		beacchiCtx.beginPath();
		beacchiCtx.clearRect(0, 0, beacchiCanvas.width, beacchiCanvas.height);
		// 素材を描画
		_.each(materials, function (elm, i) {
			elm.countFrame(); // 素材ごとの経過フレーム数を算出
			elm.refreshParam(); // 経過フレーム数に応じて移動など
			if (elm.deg === 0) {
				beacchiCtx.drawImage(elm.img, elm.imgSx, elm.imgSy, elm.imgSw, elm.imgSh, elm.imgDx, elm.imgDy, elm.imgDw, elm.imgDh);
			} else {
				// 画像編集用キャンバス初期化
				paletteCtx.beginPath();
				paletteCtx.clearRect(0, 0, paletteCanvas.width, paletteCanvas.height);
				// 画像投入
				paletteCtx.drawImage(elm.img, 0, 0);
				// 回転などのパラメータ保持
				paletteCtx.restore();
				paletteCtx.save();
				// 回転軸の定義
				var axisMoveX = 0;
				var axisMoveY = 0;
				switch (elm.axis) {
					case 'centermiddle':
						axisMoveX = elm.img.width/2;
						axisMoveY = elm.img.height/2;
						break;
					case 'centertop':
						axisMoveX = elm.img.width/2;
						axisMoveY = 0;
						break;
				}
				// 軸を中心にしてから回転し、軸を元に戻す
				paletteCtx.translate(axisMoveX, axisMoveY);
				paletteCtx.rotate(elm.deg * Math.PI / 180);
				paletteCtx.translate(-1*axisMoveX, -1*axisMoveY);
				// 回転後の画像を描画
				beacchiCtx.drawImage(paletteCanvas, elm.imgSx, elm.imgSy, elm.imgSw, elm.imgSh, elm.imgDx, elm.imgDy, elm.imgDw, elm.imgDh);
			}
		});
	}

	//**************************************************
	// 関数群
	//**************************************************
	// セリフ更新関数
	function refreshSerif () {
		// ランダムでセリフ取得
		var rand = Math.floor(Math.random() * serifData.length);
		var message = serifData[rand].message;
		var volume = serifData[rand].volume;

		// balloon DOMを作成
		var domBalloon = $("<div></div>", {id: "balloon"});
		var domMessage = $("<p></p>", {id: "message", text: message});
		domBalloon.append(domMessage);
		// 一旦DOMを削除して追加
		$("#balloon_container").empty();
		$("#balloon_container").append(domBalloon);
		domBalloon.addClass(volume);

		initializeBeacchi();
		// ベアっち表情適用
		attachBeacchiFace(serifData[rand].action);

		// 取得済みのセリフを削除
		serifData.splice(rand, 1);
		if (serifData.length == 0) {
			// セリフがもう無い！
			serifSoldOut();
		}
	}

	// セリフが使い果たされた時に呼ばれる関数
	// ※Object.observeってそのうち出来るらしい
	function serifSoldOut () {
		// セリフ補充(ディープコピー)
		serifData = null; // ガベージコレクタ動くかしら
		serifData = $.extend(true, [], serifDataOrg);
	};

	// ベアっち初期化
	function initializeBeacchi () {
		removeAllMaterial();
		addMaterial(matBeacchi);
	}

	// ベアっち表情適用
	function attachBeacchiFace (action) {
		if (typeof action !== "undefined") {
			if (action === 'kiran') {
				// メガネキラン☆
				addMaterial(matKiran);
			}
			if (action === 'tongue') {
				// あっかんべー
				addMaterial(matTongue);
			}
			if (action === 'tear') {
				// うるうる
				addMaterial(matTear1);
				addMaterial(matTear2);
			}
			if (action === 'saaa') {
				// 血の気が引く
				addMaterial(matSaaa);
			}
			if (action === 'angry') {
				// プンプン
				addMaterial(matAngry);
			}
		}
	}

	// ベアっちmousedown
	function beacchiMousedown () {
		refreshSerif();
	};

	// ベアっちmouseup
	function beacchiMouseup () {
		displaySerif();
	};

	// セリフ表示関数
	function displaySerif () {
		$("#balloon").addClass("disp");
	};

	// 描画素材追加関数
	function addMaterial (mat) {
		mat.attached();
		mat.initializeParam();
		materials.push(mat);
	}

	// 描画素材を空に
	function removeAllMaterial () {
		materials = [];
	}
});

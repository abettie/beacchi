<?php
	$css_files = array(
		"css/common.css",
		"css/game.css",
	);
	$js_files = array(
		"js/game.js",
	);
?>
<?php include("../include/template/header.tpl.php"); ?>
	<div class="container">
		<canvas id="beacchi" width=1200 height=500></canvas>
		<canvas id="palette" style="display:none;"></canvas>
	</div>
<?php include("../include/template/footer.tpl.php"); ?>
